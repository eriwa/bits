This repository contains all the sources needed to maintain the Debian blog
https://bits.debian.org and instructions to build the site.


## Contents

		Makefile
		pelicanconf.py  configuration file for pelican
		publishconf.py  publishing configuration file for pelican
		theme-bits/     contains the blog's theme and the templates used.
		content/        source files for the posts
		content/pages/  source files the static pages
		content/2013/   source files for the blog posts from 2013
		discarded/      source files for discarded blog posts
		output/         should be always empty, it is for generate the blog locally
		plugins/        pelican plugins


## Testing locally

* Install pelican

* Clone this repository

		$ git clone https://salsa.debian.org/publicity-team/bits.git && cd bits

* Build the site:

		$ make html

* Serve it locally to see if it's rendered properly:

		$ make serve

* Open <http://localhost:8000> in your browser.


## Updating the blog in dillon.d.o

This is only valid for people in the publicity group (the group of Linux
users in Debian machines).

* Log in dillon.debian.org and get a copy of the bits repository
or update your current copy:

		$ git clone https://salsa.debian.org/publicity-team/bits.git

* Execute:

		$ cd bits
		$ make mpublish

This will remove all the files at `/srv/bits-master.debian.org/htdocs`
and will regenerate a new copy of the website and propagate update
to all the static mirrors.
