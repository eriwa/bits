Title: Nous desenvolupadors i mantenidors de Debian (juliol i agost del 2016)
Slug: new-developers-2016-08
Date: 2016-09-03 12:00
Author: Jean-Pierre Giraud
Translator: Ana Guerrero Lopez
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

  * Edward John Betts (edward)
  * Holger Wansing (holgerw)
  * Timothy Martin Potter (tpot)
  * Martijn van Brummelen (mvb)
  * Stéphane Blondon (sblondon)
  * Bertrand Marc (bmarc)
  * Jochen Sprickerhof (jspricke)
  * Ben Finney (bignose)
  * Breno Leitao (leitao)
  * Zlatan Todoric (zlatan)
  * Ferenc Wágner (wferi)
  * Matthieu Caneill (matthieucan)
  * Steven Chamberlain (stevenc)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

  * Jonathan Cristopher Carter
  * Reiner Herrmann
  * Michael Jeanson
  * Jens Reyer
  * Jerome Benoit
  * Frédéric Bonnard
  * Olek Wojnar

Enhorabona a tots!






