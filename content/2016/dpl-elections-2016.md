Title: Debian Project Leader elections 2016
Date: 2016-03-27 20:55
Tags: dpl, vote
Slug: dpl-elections-2016
Author: Ana Guerrero Lopez
Status: published

It's that time of year again for the Debian Project: [the elections of
its Project Leader](https://www.debian.org/vote/2016/vote_001)!

Neil McGovern who has held the office for the last year will not be
seeking reelection. Debian Developers will have to choose between voting
for the only candidate running [Mehdi Dogguy](https://www.debian.org/vote/2016/platforms/mehdi)
or *None Of The Above*.  If *None Of The Above* wins the election then
the election procedure is repeated, many times if necessary.

Mehdi Dogguy was a candidate for the DPL position last year, finishing
second with a close amount of votes to the winner Neil McGovern.

We are in the middle of the campaigning period that will last until April
2nd. The candidate and Debian contributors are expected to engage in
debates and discussions on the [debian-vote mailing list](http://lists.debian.org/debian-vote/).

The voting period starts on April 3rd, and during the following two
weeks, Debian Developers will vote to choose the person who will guide
the project for one year. The results will be published on April 17th
with the term for new the project leader starting immediately that same
day.
