Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2022)
Slug: new-developers-2022-10
Date: 2022-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Abraham Raji (abraham)
  * Phil Morrell (emorrp1)
  * Anupa Ann Joseph (anupa)
  * Mathias Gibbens (gibmat)
  * Arun Kumar Pariyar (arun)
  * Tino Didriksen (tinodidriksen)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Gavin Lai
  * Martin Dosch
  * Taavi Väänänen
  * Daichi Fukui
  * Daniel Gröber
  * Vivek K J
  * William Wilson
  * Ruben Pollan

Enhorabona a tots!

