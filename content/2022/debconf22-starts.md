Title: DebConf22 starts today in Prizren
Slug: debconf22-starts
Date: 2022-07-17 09:00
Author: The Debian Publicity Team
Tags: debconf, debconf22
Lang: en
Image: /images/debconf22-banner-700px.png
Status: published

[DebConf22](https://debconf22.debconf.org/), the 23rd annual
Debian Conference, is taking place in Prizren, Kosovo
from July 17th to 24th, 2022.

Debian contributors from all over the world have come together at
[Innovation and Training Park (ITP)](https://debconf22.debconf.org/about/venue)
in Prizren, Kosovo, to participate and work in a conference exclusively
run by volunteers.

Today the main conference starts with over 270 attendants expected
and 82 activities scheduled,
including 45-minute and 20-minute talks and team meetings ("BoF"),
workshops, and a job fair, as well as a variety of other events.

The full schedule at
[https://debconf2.debconf.org/schedule/](https://debconf22.debconf.org/schedule/)
is updated every day, including activities planned ad-hoc
by attendees during the whole conference.

If you want to engage remotely, you can follow the
[**video streaming** available from the DebConf22 website](https://debconf22.debconf.org/)
of the events happening in the three talk rooms:
_Drini_, _Lumbardhi_ and _Ereniku_.
Or you can join the conversation about what is happening
in the talk rooms:
  [**#debconf-drini**](https://webchat.oftc.net/?channels=#debconf-drini),
  [**#debconf-lumbardhi**](https://webchat.oftc.net/?channels=#debconf-lumbardhi) and
  [**#debconf-ereniku**](https://webchat.oftc.net/?channels=#debconf-ereniku)
(all those channels in the OFTC IRC network).

You can also follow the live coverage of news about DebConf22 on
[https://micronews.debian.org](https://micronews.debian.org) or the @debian
profile in your favorite social network.

DebConf is committed to a safe and welcoming environment for all participants.
See the [web page about the Code of Conduct in DebConf22's website](https://debconf22.debconf.org/about/coc/)
for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf22.debconf.org/sponsors/)
to support DebConf22, particularly our Platinum Sponsors:
[**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**ITP Prizren**](https://itp-prizren.com)
and [**Google**](https://google.com).

![DebConf22 banner open registration](|static|/images/debconf22-banner-700px.png)
