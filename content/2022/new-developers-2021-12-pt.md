Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (novembro e dezembro de 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * Douglas Andrew Torrance (dtorrance)
  * Mark Lee Garrett (lee)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Lukas Matthias Märdian
  * Paulo Roberto Alves de Oliveira
  * Sergio Almeida Cipriano Junior
  * Julien Lamy
  * Kristian Nielsen
  * Jeremy Paul Arnold Sowden
  * Jussi Tapio Pakkanen
  * Marius Gripsgard
  * Martin Budaj
  * Peymaneh
  * Tommi Petteri Höynälänmaa

Parabéns a todos!

