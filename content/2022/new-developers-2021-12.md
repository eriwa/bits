Title: New Debian Developers and Maintainers (November and December 2021)
Slug: new-developers-2021-12
Date: 2022-01-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Douglas Andrew Torrance (dtorrance)
  * Mark Lee Garrett (lee)

The following contributors were added as Debian Maintainers in the last two months:

  * Lukas Matthias Märdian
  * Paulo Roberto Alves de Oliveira
  * Sergio Almeida Cipriano Junior
  * Julien Lamy
  * Kristian Nielsen
  * Jeremy Paul Arnold Sowden
  * Jussi Tapio Pakkanen
  * Marius Gripsgard
  * Martin Budaj
  * Peymaneh
  * Tommi Petteri Höynälänmaa

Congratulations!

