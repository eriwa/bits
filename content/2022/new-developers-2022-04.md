Title: New Debian Developers and Maintainers (March and April 2022)
Slug: new-developers-2022-04
Date: 2022-05-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Henry-Nicolas Tourneur (hntourne)
  * Nick Black (dank)

The following contributors were added as Debian Maintainers in the last two months:

  * Jan Mojžíš
  * Philip Wyett
  * Thomas Ward
  * Fabio Fantoni
  * Mohammed Bilal
  * Guilherme de Paula Xavier Segundo

Congratulations!

