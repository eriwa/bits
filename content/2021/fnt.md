Title: 2,000 fonts for Debian
Slug: 2000-fonts-debian
Date: 2021-12-17 18:45
Author: Gürkan Myczko
Artist: Gürkan Myczko
Tags: fonts, typography, artwork
Status: published

![fnt](|static|/images/fnt-poster.png)

Debian comes with tons of fonts for all kinds of purposes,
you can easily list them (almost) all with: `apt-cache search ^fonts-`.

However, sometimes they are not in their latest version, or as a user you would like
to get access to new fonts that are not present in Debian stable yet.

With the tool [`fnt`](https://github.com/alexmyczko/fnt) you can easily preview, and install
fonts from Debian sid and Google Web Fonts (that's about 2,000 fonts that are DSFG compliant).
Any user can use the tool to install fonts only for the user itself, or the
system administrator can run it as root to install the fonts system wide.

The [package `fnt`](https://packages.debian.org/bookworm/fnt) is already in Bookworm,
so if you run Debian testing you can use it to get, test and use many fonts that are in their way
of being packaged in Debian: 

[ITP #973779](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=973779)   `fnt install scheherazadenew`  
[RFP #944140](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=944140)   `fnt install arsenal`  
[RFP #757249](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=757249)   `fnt install ekmukta`  
[RFP #724629](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=724629)   `fnt install firasans`  
[RFP #766211](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=766211)   `fnt install orbitron`  
[RFP #803690](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=803690)   `fnt install pompiere`  
[RFP #754784](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=754784)   `fnt install raleway`  
[RFP #827735](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=827735)   `fnt install reemkufi`  
[RFP #986999](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=986999)   `fnt install redhat{display,mono,text}`  


If you want to learn more you can have a look at the wiki page about fonts
([https://wiki.debian.org/Fonts](https://wiki.debian.org/Fonts)),
and if you want to contribute or maintain fonts in Debian,
don't hesitate to join the [Fonts Team](https://wiki.debian.org/Teams/pkg-fonts)!
