Title: Debian 11 Bullseye est publiée !
Date: 2021-08-14 23:30
Tags: bullseye
Slug: bullseye-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Artist: Juliette Taka
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

[![Alt Bullseye est publiée](|static|/images/banner_bullseye.png)](https://deb.li/bullseye)

Nous sommes heureux d'annoncer la publication de Debian 11 *Bullseye*.

**Vous voulez l'installer ?**
Choisissez votre [support d'installation](https://www.debian.org/distrib/) préféré
puis lisez le [manuel d'installation](https://www.debian.org/releases/bullseye/installmanual).
Vous pouvez aussi utiliser une image officielle pour l'informatique dématérialisée
directement avec votre fournisseur ou essayer Debian avant de l'installer avec
nos images autonomes.

**Vous êtes déjà un utilisateur heureux de Debian, et vous voulez juste la mettre à niveau ?**
Vous pouvez facilement mettre à niveau votre installation de Debian 10 « Buster »
en consultant les [notes de publication](https://www.debian.org/releases/bullseye/releasenotes).

**Vous voulez célébrer la publication ?**
Nous vous offrons quelques [illustrations de Bullseye](https://wiki.debian.org/DebianArt/ThemesHomeworld)
que vous pouvez partager ou utiliser comme inspiration pour propres créations.
Suivez la conversation à propos de Bullseye sur les médias sociaux avec les étiquettes #ReleasingDebianBullseye
et #Debian11Bullseye ou venez nous rencontrer en personne ou en ligne dans une
[fête de publication](https://wiki.debian.org/ReleasePartyBullseye) !
