Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng chín và mười 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Bastian Germann (bage)
  * Gürkan Myczko (tar)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Clay Stan
  * Daniel Milde
  * David da Silva Polverari
  * Sunday Cletus Nkwuda
  * Ma Aiguo
  * Sakirnth Nagarasa

Xin chúc mừng!

