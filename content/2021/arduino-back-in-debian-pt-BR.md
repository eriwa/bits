Title: Arduino está de volta ao Debian
Slug: arduino-back-in-debian
Date: 2021-02-01 08:00
Author: Rock Storm
Tags: arduino, announce
Lang: pt-BR
Translator: Carlos Henrique Lima Melara, Thiago Pezzo (Tico), Paulo Henrique de Lima Santana (phls)
Status: published

A equipe Debian Eletrônica está feliz em anunciar que a versão mais recente
do Arduino, provavelmente a mais disseminada plataforma de programação de
microcontroladores AVR, está agora [empacotada e disponível no Debian instável
(unstable)][1].

A última versão do Arduino que estava disponível no Debian era a 1.0.5, a qual
foi lançada em 2013. Foram anos de tentativas e falhas, mas finalmente, após
um grande esforço de vários meses de Carsten Schoenert e Rock Storm, nós temos
um pacote funcional da mais recente versão do Arduino. Após 7 anos, será
possível instalar a IDE Arduino simplesmente digitando *"apt install arduino"*
novamente.

*"O propósito desta publicação não é somente anunciar a disponibilidade do
pacote, mas também solicitar, na verdade, que ele seja testado"* disse Rock
Storm. *" O título poderia muito bem ter sido _Procura-se: beta testers para
Arduino_ (mortos ou vivos :P)."*. A equipe Debian Eletrônica apreciaria
se pessoas com as ferramentas e o conhecimento necessário puderem testar o
pacote e informar se forem encontrados quaisquer problemas.

Com esta publicação, agradecemos a equipe Debian Eletrônica e todos(as)
os(as) contribuidores(as) do pacote. Este feito não seria possível sem a
contribuição de vocês.

[1]: https://packages.debian.org/unstable/arduino
