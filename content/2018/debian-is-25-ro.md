Title: 25 de ani și numărătoarea continuă
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Lang: ro
Translator: Nicolae Crefelean
Status: published

![Debian a împlinit 25 de ani, de Angelo Rosa](|static|/images/debian25years.png)

Când răposatul Ian Murdock a anunțat acum 25 de ani, în comp.os.linux.development,
*„finalizarea iminentă a unei lansări complet noi Linux, [...] Lansarea Debian Linux”*,
nimeni nu s-ar fi așteptat ca „Lansarea Debian Linux” să devină ceea ce știm astăzi ca
Proiectul Debian, unul dintre cele mel mari și influente proiecte de software liber.
Principalul său produs este Debian, un sistem de operare (SO) liber pentru calculatorul dvs.,
precum și pentru multe alte sisteme care vă îmbunătățesc viața. De la operațiunile interne
ale aeroportului din apropiere, până la sistemul multimedia al mașinii dvs.,
și de la serverele din cloud care găzduiesc saiturile dvs. preferate,
la dispozitivele IoT care comunică cu acestea, Debian le poate susține pe toate.

Astăzi, proiectul Debian este o organizație mare și înfloritoare cu nenumărate echipe care
se gestionează autonom, alcătuite din voluntari. Deși adeseori pare haotic privind din exterior,
proiectul este susținut de cele două documente organizaționale: [Contractul Social Debian],
care oferă o viziune de îmbunătățire a societății, și [Ghidul pentru Software-ul Liber Debian],
care oferă indicații despre ce fel de software este considerat util. Acestea sunt completate de
[Constituția proiectului], care definește
structura proiectului, și [Codul de Conduită], care definește tonul interacțiunilor din cadrul proiectului.

În fiecare zi din ultimii 25 de ani, utilizatorii ne-au raportat erori și au oferit
remedii pentru acestea, au încărcat pachete, au actualizat traduceri, au creat grafică,
au organizat evenimente despre Debian, au actualizat saitul, i-au învățat pe alții
cum să folosească Debian și au create sute de derivate.

**Să cinstim pentru încă 25 de ani - și sperăm la cât mai mulți!**

[Contractul Social Debian]:  https://www.debian.org/social_contract
[Ghidul pentru Software-ul Liber Debian]:  https://www.debian.org/social_contract#guidelines
[Constituția proiectului]:  https://www.debian.org/devel/constitution
[Codul de Conduită]: https://www.debian.org/code_of_conduct

