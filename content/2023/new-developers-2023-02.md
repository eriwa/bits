Title: New Debian Developers and Maintainers (January and February 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Sahil Dhiman (sahil)
  * Jakub Ružička (jru)

The following contributors were added as Debian Maintainers in the last two months:

  * Josenilson Ferreira da Silva
  * Ileana Dumitrescu
  * Douglas Kosovic
  * Israel Galadima
  * Timothy Pearson
  * Blake Lee
  * Vasyl Gello
  * Joachim Zobel
  * Amin Bandali

Congratulations!

