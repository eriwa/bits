Title: नए डेबियन डेवलपर्स और मेंटेनर्स (जनवरी और फ़रवरी 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published


पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

  * Sahil Dhiman (sahil)
  * Jakub Ružička (jru)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

  * Josenilson Ferreira da Silva
  * Ileana Dumitrescu
  * Douglas Kosovic
  * Israel Galadima
  * Timothy Pearson
  * Blake Lee
  * Vasyl Gello
  * Joachim Zobel
  * Amin Bandali

इन्हें बधाईयाँ!

