Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (janeiro e fevereiro de 2023)
Slug: new-developers-2023-02
Date: 2023-03-22 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * Sahil Dhiman (sahil)
  * Jakub Ružička (jru)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Josenilson Ferreira da Silva
  * Ileana Dumitrescu
  * Douglas Kosovic
  * Israel Galadima
  * Timothy Pearson
  * Blake Lee
  * Vasyl Gello
  * Joachim Zobel
  * Amin Bandali

Parabéns a todos!

