Title: Nya Debianutvecklare och Debian Maintainers (Mars och April 2020)
Slug: new-developers-2020-04
Date: 2020-05-28 18:30
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Paride Legovini (paride)
  * Ana Custura (acute)
  * Felix Lechner (lechner)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Sven Geuer
  * Håvard Flaget Aasen

Grattis!

