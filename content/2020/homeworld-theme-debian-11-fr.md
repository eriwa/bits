Title: « Homeworld » : le prochain thème par défaut pour Debian 11
Slug: homeworld-will-be-the-default-theme-for-debian-11
Date: 2020-11-12 13:30
Author: Jonathan Carter
Artist: Juliette Taka
Tags: bullseye, artwork
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

Le thème ["Homeworld"][homeworld-link] de Juliette Taka a été choisi comme thème
par défaut de Debian 11 « Bullseye ». Juliette dit qu'elle s'est inspirée pour
ce thème du mouvement Bauhaus, un courant artistique né en Allemagne au
20ᵉ siècle.

[![Fond d'écran Homeworld. Cliquez pour voir la totalité du thème proposé](|static|/images/homeworld_wallpaper.png)][homeworld-link]

[![Thème de l'installateur Debian. Cliquez pour voir la totalité du thème proposé](|static|/images/homeworld_debian-installer.png)][homeworld-link]

[homeworld-link]: https://wiki.debian.org/DebianArt/Themes/Homeworld

Après [l'appel à propositions pour le thème de Bullseye](https://lists.debian.org/debian-devel-announce/2020/08/msg00002.html)
lancé par l'équipe Debian Desktop, un total de
[dix-huit offres](https://wiki.debian.org/DebianDesktop/Artwork/Bullseye)
ont été soumises. Le vote pour le thème a été ouvert au public et 5613 réponses
classant les différentes propositions ont été reçues et c'est
« Homeworld » qui a été classé premier.

C'est la troisième fois que la soumission de Juliette remporte le concours.
Juliette est également l'auteure du thème [lines](https://wiki.debian.org/DebianArt/Themes/Lines)
utilisé pour Debian 8 et du thème [softWaves theme](https://wiki.debian.org/DebianArt/Themes/softWaves)
choisi pour Debian 9.

Nous remercions tous les graphistes qui ont participé et proposé leur excellent
travail sous la forme de fonds d'écran et de thème graphique pour Debian 11.

Félicitations Juliette, et merci beaucoup pour ta contribution à Debian !
