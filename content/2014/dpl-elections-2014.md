Title: Debian Project Leader elections 2014
Date: 2014-03-21
Tags: dpl, vote
Slug: dpl-elections-2014
Author: Francesca Ciceri
Status: published

It's again that time of the year for the Debian Project: [the elections of its
Project Leader](https://www.debian.org/vote/2014/vote_001)!  Starting on March
31st, and during the following two weeks, the Debian Developers will vote to
choose the person who will guide the project for one year.

Among this year's candidates there is the current DPL, Lucas Nussbaum, who
admits that "*the workload involved in being the DPL is just
huge,*" and motivates his nomination with the need for stability in the
project in this release cycle, especially after the difficult decision about
the default init system.  In [his
platform](https://www.debian.org/vote/2014/platforms/lucas), Lucas speaks of
technical and social steps to improve the project: from [reproducible
builds](https://wiki.debian.org/ReproducibleBuilds) for a more secure archive
to a renewed effort to run Debian on new platforms (especially smartphone and
tablets); from a more welcoming approach to prospective contributors to an
easier collaboration with organizations.

The only other candidate left after [Gergely Nagy withdrew his
nomination](https://lists.debian.org/debian-vote/2014/03/msg00174.html), is
former Release Manager Neil McGovern.  [Neil's
platform](https://www.debian.org/vote/2014/platforms/neilm) focuses mainly on the
need to "*ensure that we cater to our users, and there's millions of
them. From those running the latest software in unstable, to people who simply
want a rock solid core release.*" In his opinion "*the size of
Debian is increasing, and will reach a point where we're unable to guarantee
basic compatibility with other packages, or the length of time it takes to do
so becomes exponentially longer, unless something changes*." To fix this
problem, Neil proposes the implementation of PPAs ([Personal Package
Archives](http://en.wikipedia.org/wiki/Personal_Package_Archive)), the
modernisation of the current build and infrastructure system as well as
generally supporting the various teams.

The campaigning period will last until March 30th: the candidates are already
engaged in debates and discussions on the [debian-vote mailing
list](http://lists.debian.org/debian-vote/) where they'll reply to questions from
users and contributors.
