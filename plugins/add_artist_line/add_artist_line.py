"""
This plugin add the `artist` value to the article
"""

from pelican import signals

def add_artist_line(generator):

    for article in generator.articles:
        if hasattr(article,'artist'):
            artist = article.artist

def register():
    signals.article_generator_finalized.connect(add_artist_line)
